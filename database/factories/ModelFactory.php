<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
// Фабрика для создания случайных рабов
$factory->define(App\Slave::class, function (Faker\Generator $faker) {
    $gender = mt_rand(0, 1);

    return [
        'nickname' => $gender ? $faker->firstNameFemale : $faker->firstNameMale,
        'gender' => $gender,
        'weight' => mt_rand(30, 100),
        'skin_color' => $faker->randomElement(['white', 'black', 'yellow']),
        'birthday' => $faker->dateTimeBetween('-40 years', '-10 years'),
        'birthplace' => $faker->city,
        'rental_rate' => mt_rand(1, 50),
        'cost' => mt_rand(500, 50000),
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->colorName,
    ];
});

$factory->define(App\RentalRecord::class, function (Faker\Generator $faker) {
    $date_start = $faker->dateTimeBetween('-2 weeks', '2 month');
    $date = new \Carbon\Carbon($date_start->format('Y-m-d H:i:s'));
    $date_end = $faker->dateTimeBetween($date->addHour()->toDateString(), $date->addMonths(2)->toDateString());
    return [
        'client' => $faker->name,
        'slave_id' => 261,
        'date_start' => $date_start,
        'date_end' => $date_end,
        'work_hours' => 0,
        'cost' => 0,
        'user_cost' => 0,
        'currency' => 0
    ];
});