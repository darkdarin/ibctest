<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Создаем случайных рабов
        factory(App\Slave::class, 500)->create();

        // Создаем случайное дерево категорий
        factory(App\Category::class, 10)->create()->each(function ($root) {
            /** @var App\Category $root */
            $this->createChild($root, 1);
        });

        // На всякий случай пересоздаем все ключи дерева
        App\Category::rebuild();

        // Назначаем всем рабам случайные категории
        $slaves = App\Slave::pluck('id');
        $categories = App\Category::pluck('id');

        foreach ($slaves as $slave) {
            for ($i = 0; $i < mt_rand(1, 4); $i++) {
                $category = $categories[mt_rand(0, count($categories) - 1)];
                if (!DB::table('slaves_categories')->where('slave_id', $slave)->where('category_id', $category)->first()) {
                    DB::table('slaves_categories')->insert([
                        'slave_id' => $slave,
                        'category_id' => $category
                    ]);
                }
            }
        }

        // Создадим несколько записей аренды
        factory(App\RentalRecord::class, 50)->create();
    }

    /**
     * @param App\Category $parent
     * @param int $level
     */
    public function createChild($parent, $level)
    {
        $count = mt_rand(0, 5);
        if ($level > 2 || $count < 1) return;
        if ($count == 1) $count = 2;

        $categories = factory(App\Category::class, $count)->create();

        foreach ($categories as $child) {
            /** @var App\Category $child */
            $child->makeChildOf($parent);

            $this->createChild($child, $level + 1);
        };
    }
}
