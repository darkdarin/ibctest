<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentalRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client');
            $table->integer('slave_id');
            $table->dateTime('date_start');
            $table->dateTime('date_end');
            $table->integer('work_hours');
            $table->integer('cost');
            $table->integer('user_cost');
            $table->integer('currency');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rental_records');
    }
}
