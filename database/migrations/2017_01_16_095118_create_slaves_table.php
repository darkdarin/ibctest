<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slaves', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nickname', 100);
            $table->unsignedTinyInteger('gender');
            $table->integer('weight');
            $table->string('skin_color', 15);
            $table->date('birthday');
            $table->string('birthplace', 200)->nullable();
            $table->text('description')->nullable();
            $table->integer('rental_rate');
            $table->integer('cost');
            $table->timestamp('sold_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slaves');
    }
}
