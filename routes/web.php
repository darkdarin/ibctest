<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
\Carbon\Carbon::setLocale('ru');
setlocale(LC_TIME, 'ru_RU.UTF-8');

// навигация по рабам
Route::get('/', 'IndexController@index');
Route::get('/category/{id}', 'IndexController@category');

// покупка рабов
Route::get('/buy/{id}', 'BuyController@index');
Route::post('/buy/{id}', 'BuyController@confirm');
Route::get('/buy/{id}/buy', 'BuyController@buy');

// аренда рабов
Route::get('/rent/{id}', 'RentController@index');
Route::post('/rent/{id}', 'RentController@confirm');
Route::get('/rent/{id}/rent', 'RentController@rent');
