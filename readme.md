# Инструкция по установке
* Подгружаем зависимости `composer update`
* Папка `storage` должна быть доступна для записи
* Актуализируем настройки подключения к БД в файле `.env`
* Применяем миграции `php artisan migrate`
* Заполняем БД тестовыми данными `php artisan db:seed`

# Примечания
В качестве фреймворка выбран Laravel по личным предпочтениям - на мой взгляд данный фреймворк предоставляет 
очень легкий, удобный и быстрый интерфейс дял разработки практически любых web-приложений.

Для хранения дерева категорий используется NestedSets. Небольшая избыточность данных позволяет минимальным 
количеством запросов (чаще всего одним) получать всю информацию о дереве - предки, наследники элемента, всю ветку, 
в которой содержится элемент, соседей и многое другое.
NestedSets реализуется с помощью пакета `baum/baum` для Laravel.

Все возможные валюты, а также их курсы хранятся в файле настроек `config/currency.php`. 
Данная конфигурация позволяет легко добавлять новые виды валюты без вмешательства в код.

# Задания по MySQL

### Получить минимальную, максимальную и среднюю стоимость всех рабов весом более 60 кг.
`SELECT MIN(cost), MAX(cost), AVG(cost) FROM slaves WHERE weight > 60;`

### Выбрать категории, в которых больше 10 рабов.
`SELECT a.name, COUNT(b.slave_id) FROM categories a
JOIN slaves_categories b
  ON b.category_id = a.id
GROUP BY a.id
HAVING COUNT(b.slave_id) > 10;`

### Выбрать категорию с наибольшей суммарной стоимостью рабов.
`SELECT c.id, c.name FROM categories c
  JOIN slaves_categories sc
    ON sc.category_id = c.id
  JOIN slaves s
    ON s.id = sc.slave_id
GROUP BY sc.category_id
HAVING SUM(s.cost) >= ALL(
   SELECT SUM(s.cost) as sum_cost FROM slaves s
     JOIN slaves_categories sc
       ON s.id = sc.slave_id
   GROUP BY sc.category_id
)
ORDER BY NULL;`

### Выбрать категории, в которых мужчин больше чем женщин.
`SELECT c.id, c.name,
  count(IF (s.gender=0, 1, NULL)) as man_count,
  count(IF (s.gender=1, 1, NULL)) as woman_count
FROM categories c
  JOIN slaves_categories sc
    ON sc.category_id = c.id
  JOIN slaves s
    ON s.id = sc.slave_id
GROUP BY sc.category_id
HAVING man_count > woman_count;`


### Количество рабов в категории "Для кухни" (включая все вложенные категории).
`SELECT COUNT(DISTINCT s.id) FROM slaves s
JOIN slaves_categories sc
ON sc.slave_id = s.id AND sc.category_id IN (
    SELECT c.id FROM categories c
      LEFT JOIN categories parent
        ON parent.id = 5
    WHERE c.left_key BETWEEN parent.left_key AND parent.right_key
)`
