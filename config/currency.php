<?php

return [

    'list' => [
        0 => [
            'name' => 'Золото',
            'abbreviation' => 'зол.',
            'count' => 1,
            'rate' => 1
        ],
        1 => [
            'name' => 'Овцы',
            'abbreviation' => 'ов.',
            'count' => 1,
            'rate' => 3,
        ],
        2 => [
            'name' => 'Специи',
            'abbreviation' => 'унц.сп.',
            'count' => 10,
            'rate' => 1
        ]
    ]

];
