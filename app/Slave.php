<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Slave
 * Модель "Раб". Содержит базовую информацию о рабе. Может находиться во многих категориях.
 * Используется SoftDelete для пометки проданных рабов.
 * @package App
 * @property integer $id
 * @property string $nickname
 * @property integer $gender
 * @property integer $weight
 * @property string $skin_color
 * @property Carbon $birthday
 * @property string $birthplace
 * @property string $description
 * @property integer $rental_rate
 * @property integer $cost
 * @property Carbon $sold_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Slave extends Model
{
    use SoftDeletes;

    const DELETED_AT = 'sold_at';

    public $table = 'slaves';

    public $dates = ['birthday', 'sold_at'];

    /**
     * Возвращает все категории, к которым относится данный раб
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'slaves_categories');
    }
}
