<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RentalRecord
 * Модель записи аренды.
 * Содержит информацию о заказчике и заказе. Также хранит информацию о сумме заказа
 * (если цена раба изменится - мы не можем менять цену в уже заказанной аренде)
 * @package App
 * @property int $id
 * @property string $client
 * @property int $slave_id
 * @property Carbon $date_start
 * @property Carbon $date_end
 * @property int $work_hours
 * @property int $cost
 * @property int $user_cost
 * @property int $currency
 */
class RentalRecord extends Model
{
    const WORK_DAY = 16;

    public $table = 'rental_records';

    public $dates = ['date_start', 'date_end'];

    /**
     * Проверяет, можно ли арендовать раба на выбранное время
     * @return array Массив ошибок
     */
    public function isRentalTime()
    {
        $errors = [];
        $date_start = $this->date_start;
        $date_end = $this->date_end;

        /** @var RentalRecord[] $rents */
        $rents = RentalRecord::where('slave_id', $this->slave_id)
            ->where(function ($query) use ($date_start, $date_end) {
                $query->where(function ($query) use ($date_start, $date_end) {
                    // если время старта аренды находится в промежутке брони
                    $query->where('date_start', '>=', $date_start)
                        ->where('date_start', '<', $date_end);
                })->orWhere(function($query) use ($date_start, $date_end) {
                    $query->where('date_end', '>', $date_start)
                        ->where('date_end', '<=', $date_end);
                    // если время окончания аренды находится в промежутке брони
                })->orWhere(function($query) use ($date_start, $date_end) {
                    // если вся бронь попадает в чью-то аренду
                    $query->where('date_start', '<=', $date_start)
                        ->where('date_end', '>=', $date_end);
                });
            })->get();

        // выводим ошибки, если время аренды пересекается с бронью
        foreach ($rents as $rent) {
            if ($rent->date_start <= $date_start && $rent->date_end >= $date_end) {
                $errors[] = 'Выбранный промежуток уже полностью занят чужой арендой (с ' . $rent->date_start->format('d.m.Y H:i') . ' до ' . $rent->date_end->format('d.m.Y H:i') . ')';
            } elseif ($rent->date_start >= $date_start && $rent->date_start < $date_end) {
                $errors[] = 'Уже есть бронь с ' . $rent->date_start->format('d.m.Y H:i') . '. Перенесите дату окончания аренды на это время, либо выберите другое время аренды.';
            } elseif ($rent->date_end > $date_start && $rent->date_end <= $date_end) {
                $errors[] = 'Уже есть бронь до ' . $rent->date_end->format('d.m.Y H:i') . '. Перенесите дату начала на это время, либо выберите другое время аренды.';
            }
        }
        return $errors;
    }

    public function getRentalTime()
    {
        // получаем начало дня для времени стата и окончания
        $date_start_clear = $this->date_start;
        $date_start_clear->startOfDay();
        $date_end_clear = $this->date_end;
        $date_end_clear->startOfDay();

        // время окончания и время окончания на начало дня не равны - значит захвачен еще один день
        if ($this->date_end > $date_end_clear) {
            $date_end_clear->addDay();
        }

        // если аренда на несколько дней
        if ($date_end_clear->diffInDays($date_start_clear) > 1) {
            // считаем количество чистых дней
            $days = $date_end_clear->diffInDays($date_start_clear);
            return $days * self::WORK_DAY;
        } else {
            // аренда на один день
            if ($this->date_end->diffInHours($this->date_start) > self::WORK_DAY) {
                return false;
            } else {
                return $this->date_end->diffInHours($this->date_start);
            }
        }
    }
}
