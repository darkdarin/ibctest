<?php

namespace App\Http\Controllers;

use App\Category;
use App\Slave;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Главная страница с выбором категорий
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // получим рутовые категории
        $categories = Category::roots()->get();

        return view('index', [
            'categories' => $categories,
        ]);
    }

    /**
     * Страница категории
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category($id)
    {
        // получим рутовые категориии
        $categories = Category::roots()->get();

        // получим выбранную категорию
        /** @var Category $category */
        $category = Category::find($id);
        // получим ID категорий-предков выбранной
        $branchActive = $category->ancestorsAndSelf()->pluck('id');
        // получим ID категорий-наследников выбранной
        $branchSlaves = $category->descendantsAndSelf()->pluck('id');

        // получим рабов в текущей категории,а также во всех наслдениках текущей категории
        $slaves = Slave::with('categories')->join('slaves_categories', 'slaves.id', '=', 'slaves_categories.slave_id')
            ->whereIn('slaves_categories.category_id', $branchSlaves)->orderBy('id')->get();

        return view('index', [
            'categories' => $categories,
            'branchActive' => $branchActive,
            'slaves' => $slaves
        ]);
    }
}
