<?php

namespace App\Http\Controllers;

use App\RentalRecord;
use App\Slave;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RentController extends Controller
{
    protected $errors = [];
    protected $values = [];

    /**
     * Страница аренды
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $id)
    {
        $slave = Slave::findOrFail($id);

        $freeTime = Carbon::now();
        /** @var RentalRecord[] $rentRecords */
        $rentRecords = RentalRecord::where('slave_id', $id)->where('date_end', '>', $freeTime)->get();

        // рассчитываем ближайшее свободное время
        foreach ($rentRecords as $record) {
            if ($record->date_start <= $freeTime) $freeTime = $record->date_end;
        }

        $values = $request->session()->get('values');
        if (!isset($values['date_start'])) $values['date_start'] = $freeTime->format('d.m.Y H:i');
        if (!isset($values['date_end'])) $values['date_end'] = $freeTime->addWeeks(2)->format('d.m.Y H:i');

        return view('rent', [
            'slave' => $slave,
            'freeTime' => $freeTime,
            'currencyRate' => config('currency.list'),
            'errors' => $this->errors,
            'values' => $values
        ]);
    }

    /**
     * Страница подтверждения заказа
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm(Request $request, $id)
    {
        if (!$request->input()) return redirect('/buy/' . $id);

        $request->session()->set('values', $request->input());

        // получаем данные от пользователя
        $client = $request->input('client');
        $currency = $request->input('currency');

        $date_start = $request->input('date_start');
        $date_end = $request->input('date_end');

        // проверяем валидность
        if (!strlen($date_start)) $this->errors[] = 'Пожалуйста, укажите время начала аренды';
        if (!strlen($date_end)) $this->errors[] = 'Пожалуйста, укажите время окончания аренды';
        $date_start = new Carbon($date_start);
        $date_end = new Carbon($date_end);

        if ($date_start >= $date_end) $this->errors[] = 'Время окончания аренды должно быть позже времени начала.';

        if (!strlen($client)) $this->errors[] = 'Пожалуйста, укажите свое полное имя';
        if (!isset(config('currency.list')[$currency])) $this->errors[] = 'Выберите, чем вы хотите расплачиваться, из списка';

        // проверим, что на выбранное время можно арендовать раба
        $rent = new RentalRecord();
        $rent->slave_id = $id;
        $rent->date_start = $date_start;
        $rent->date_end = $date_end;
        $errors = $rent->isRentalTime();
        $this->errors = array_merge($this->errors, $errors);

        $work_hours = $rent->getRentalTime();
        if ($work_hours === false) $this->errors[] = 'К сожалению, раб не может работать больше 16 часов в сутки.';
        if ($work_hours === 0) $this->errors[] = 'Даты начала и окончания аренды совпадают.';

        if (count($this->errors)) {
            return $this->index($request, $id);
        }

        $rateCurrency = config('currency.list')[$currency];

        /** @var Slave $slave */
        $slave = Slave::findOrFail($id);

        // создаем заказ
        $order = new \stdClass();
        $order->client = $client;
        $order->currency = $currency;
        $order->slave_id = $slave->id;
        $order->work_hours = $work_hours;
        $order->cost = $slave->rental_rate * $order->work_hours;
        $order->user_cost = round($order->cost * $rateCurrency['count'] / $rateCurrency['rate']);
        $order->date_start = $date_start;
        $order->date_end = $date_end;

        $request->session()->set('order', $order);

        // отображаем информацию о заказе
        return view('confirm', [
            'type' => 'rent',
            'slave' => $slave,
            'order' => $order,
            'currencyRate' => $rateCurrency,
        ]);
    }

    /**
     * Страница успеха
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function rent(Request $request, $id)
    {
        $order = $request->session()->get('order');

        $slave = Slave::findOrFail($id);

        // создаем запись в журнале аренды
        $record = new RentalRecord();
        $record->client = $order->client;
        $record->slave_id = $order->slave_id;
        $record->cost = $order->cost;
        $record->user_cost = $order->user_cost;
        $record->currency = $order->currency;
        $record->work_hours = $order->work_hours;
        $record->date_start = $order->date_start;
        $record->date_end = $order->date_end;
        $record->save();

        // показываем, что все ок
        return view('success', [
            'type' => 'rent',
            'slave' => $slave,
            'order' => $order,
        ]);
    }
}
