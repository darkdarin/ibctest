<?php

namespace App\Http\Controllers;

use App\RentalRecord;
use App\Slave;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BuyController extends Controller
{
    protected $errors = [];

    /**
     * Страница покупки раба
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, $id)
    {
        $slave = Slave::findOrFail($id);

        $freeTime = Carbon::now();
        $notNow = false;

        // посмотрим, когда закончатся все арнеды раба
        /** @var RentalRecord $rentRecord */
        $rentRecord = RentalRecord::where('slave_id', $id)->where('date_end', '>', $freeTime)->orderBy('date_end')->first();
        if ($rentRecord) {
            $freeTime = $rentRecord->date_end;
            $notNow = true;
        }

        return view('buy', [
            'slave' => $slave,
            'freeTime' => $freeTime,
            'notNow' => $notNow,
            'currencyRate' => config('currency.list'),
            'errors' => $this->errors,
            'values' => $request->session()->get('values')
        ]);
    }

    /**
     * Страница подтверждения покупки
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function confirm(Request $request, $id)
    {
        if (!$request->input()) return redirect('/buy/' . $id);

        $request->session()->set('values', $request->input());

        // получаем данные от пользователя
        $client = $request->input('client');
        $currency = $request->input('currency');

        // проверяем валидность
        if (!strlen($client)) $this->errors[] = 'Пожалуйста, укажите свое полное имя';
        if (!isset(config('currency.list')[$currency])) $this->errors[] = 'Выберите, чем вы хотите расплачиваться, из списка';

        if (count($this->errors)) {
            return $this->index($request, $id);
        }

        $rateCurrency = config('currency.list')[$currency];

        $slave = Slave::findOrFail($id);

        // создаем заказ
        $order = new \stdClass();
        $order->client = $client;
        $order->currency = $currency;
        $order->slave_id = $slave->id;
        $order->cost = $slave->cost;
        $order->user_cost = round($slave->cost * $rateCurrency['count'] / $rateCurrency['rate']);

        $request->session()->set('order', $order);

        return view('confirm', [
            'type' => 'buy',
            'slave' => $slave,
            'order' => $order,
            'currencyRate' => $rateCurrency,
        ]);
    }

    /**
     * Страница успеха
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function buy(Request $request, $id)
    {
        $order = $request->session()->get('order');

        $slave = Slave::findOrFail($id);
        // у нас нет никаких журналов покупок, потому просто применим SoftDelete,
        // чтобы в дальнейшем этот раб не отображался в списке и его нельзя было повторно арендовать или купить
        $slave->delete();

        return view('success', [
            'type' => 'buy',
            'slave' => $slave,
            'order' => $order,
        ]);
    }
}
