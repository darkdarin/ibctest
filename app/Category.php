<?php

namespace App;

use Baum\Node;

/**
 * Class Category
 * Модель "Категория"
 * Использует NestedSets для хранения древовидной структуры
 * @package App
 */
class Category extends Node
{
    protected $table = 'categories';

    protected $parentColumn = 'parent_id';

    protected $leftColumn = 'left_key';

    protected $rightColumn = 'right_key';

    protected $depthColumn = 'depth';

    protected $guarded = array('id', 'parent_id', 'left_key', 'right_key', 'depth');

    /**
     * Возвращает всех рабов в данной категории
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function slaves()
    {
        return $this->belongsToMany(Slave::class, 'slaves_categories');
    }
}
